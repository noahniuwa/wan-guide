import Layout from "../components/layout"


const CrewFire = () =>{
  return (
    <Layout>
      
    <div className="text_container">
      
      <h2>Wanchain&rsquo;s CrewFire&nbsp;Guide</h2>
      <br></br>
      <p>This article will explain what Crewfire is, and how to get started.</p>
      <br></br>
      <h1>What is CrewFire?</h1>
      <br></br>
      <p name="0264" class="graf graf--p">CrewFire is the leading brand ambassador management software.</p>
      <p>Through the CrewFire brand ambassador program, you&rsquo;ll be able to:</p>
      <br></br>
      <ul>
      <li>Connect and form relationships with your fellow brand ambassadors</li>
      <li>Help Wanchain reach new people and transform the business</li>
      <li>Earn points, rewards &amp; commissions</li>
      <li>Connect with and get recognition from the Wanchain founding team</li>
      </ul>
      <br></br>
      <h3>Your role as a brand ambassador:</h3>
      <br></br>
      <ul>
      <li>Like &amp; comment on Wanchain Twitter posts (you&rsquo;ll be notified when new posts arise!)</li>
      <li>Share links and graphics from CrewFire on Twitter when you get notified of new share opportunities</li>
      <li>Create &amp; share original content on social media based on instructions and briefs you&rsquo;ll receive from us through CrewFire</li>
      </ul>
      <br></br>
      <p>All along the way, you&rsquo;ll be earning points, which you&rsquo;ll be able to exchange for rewards from our rewards store!</p>
      <br></br>
      <h1>Reward system for Wanchain ambassadors</h1>
      <br></br>
      <p>Each time a new activity is posted, the points will be adjusted to the current price of WAN. The conversion rate between points and WAN, however, will remain constant. The reward tiers reward you for participating over a longer time period, as the exchange rate gets better with the more points you exchange at any one time.</p>
      <p>Reward tiers in WAN (These conversion rates will not change, however, the number of points awarded for activities will be modified according to WAN price changes):</p>
      <br></br>
      <p>10 WAN &mdash; 625 points</p>
      <p>20 WAN &mdash; 1125 points (-10% vs tier 1)</p>
      <p>30 WAN &mdash; 1535 points (-9% vs tier 2)</p>
      <p>40 WAN &mdash; 1875 points (-8% vs tier 3)</p>
      <p>50 WAN &mdash; 2185 points (-7% vs tier 4)</p>
      <p>60 WAN &mdash; 2465 points (-6% vs tier 5)</p>
      <p>70 WAN &mdash; 2735 points (-5% vs tier 6)</p>
      <p>80 WAN &mdash; 3000 points (-4% vs tier 7)</p>
      <p>90 WAN &mdash; 3270 points (-3% vs tier 8)</p>
      <p>100 WAN &mdash; 3550 points (-2% vs tier 9)</p>
      <br></br>
      <p>When setting point values for a NEW activity, we will set according to this scale:</p>
      <br></br>
      <p>$00.50 per 62.5 point</p>
      <p>$00.50 per 56.25 point</p>
      <p>$00.50 per 51.17 point</p>
      <p>$00.50 per 46.875 point</p>
      <p>$00.50 per 43.7 point</p>
      <p>$00.50 per 41.084 point</p>
      <p>$00.50 per 39.07 point</p>
      <p>$00.50 per 37.5 point</p>
      <p>$00.50 per 36.34 point</p>
      <p>$00.50 per 35.5 point</p>
      <p>$00.50 per 33.33 point</p>
      <br></br>
      <p>Points to WAN conversion at TODAY&rsquo;s prices, September 2nd, 2020 (These prices will be updated along with changes in price):</p>
      <br></br>
      <p>1 WAN per 62.5 point</p>
      <p>1 WAN per 56.25 point</p>
      <p>1 WAN per 51.17 point</p>
      <p>1 WAN per 46.875 point</p>
      <p>1 WAN per 43.7 point</p>
      <p>1 WAN per 41.084 point</p>
      <p>1 WAN per 39.07 point</p>
      <p>1 WAN per 37.5 point</p>
      <p>1 WAN per 36.34 point</p>
      <p>1 WAN per 35.5 point</p>
      <p>1 WAN per 33.33 point</p>
      <br></br>
      <p>In addition to these prizes, there will be a monthly SUPER SECRET SPECIAL PRIZE for whoever gets the most points that month!</p>
      <br></br>
      <h1>How to use CrewFire</h1>
      <br></br>
      <p><strong>1.</strong> Sign-up using <a href="https://www.crewfire.com/signup/MJW6dlXsg" data-href="https://www.crewfire.com/signup/MJW6dlXsg" class="markup--anchor markup--p-anchor" rel="noopener" target="_blank">this</a> link</p>
      <p><strong>2.</strong> Fill out the form</p>
      <p><strong>3.</strong> Click on sign-up</p>
      <br></br>
      <p>Your application will now be reviewed by the team, when your application is approved you will be added to the Wanchain community.</p>
      <br></br>
      <p>When there is a new assignment you will receive an email (make sure to remove the CrewFire mail address from your spam list). Additionally, the assignments will be posted on the Telegram groups.</p>
      <br></br>
      <img src="cfactivities.png"></img>
      <br></br>
      <br></br>
      <p>Click on the assignment, read the instructions, and perform the provided tasks. When this is done click on submit content.</p>
      <br></br>
      <img src="cfactivities2.png"></img>
      <br></br>
      <br></br>
      <p>Now leave the link to your Twitter profile in the designated field and click on submit content again.</p>
      <br></br>
      <img src="cfpostsubmitted.png"></img>
      <br></br>
      <br></br>
      <p>Your post is now submitted and will be reviewed. If all requirements are met you will receive the points.</p>
      <br></br>
      <h1>Rankings</h1>
      <br></br>
      <p>The person with the most points at the end of the month will receive an additional reward. The reward differs every month. You can check your ranking on the &lsquo;Rankings&rsquo; tab.</p>
      <br></br>
      <h1>Rewards</h1>
      <br></br>
      <img src="cfrewards.png"></img>
      <br></br>
      <br></br>
      <p>On the rewards tab, you can redeem rewards for your points. Rewards will be sent to your wallet address. How this is done will be specified later. The Paypal option is not applicable for rewards.</p>
      <br></br>
      <h1>Profile and Settings</h1>
      <br></br>
      <img src="cfprofiles.png"></img>
      <br></br>
      <br></br>
      <p>On the profile tab, you can see the points that you received for each submission.</p>
      <br></br>
      <img src="cfsettings.png"></img>
      <br></br>
      <br></br>
      <p>In settings, you can change your profile, connect your social media accounts, and log out.</p>
    </div>
   

    {/* All CSS rules are defined in the style tag. 
    They will be scoped locally to this component so
    you don't need to worry about naming conflicts*/}
    <style jsx>{`    
      .hidden {
        display: none;
      }
      .text_container {
        margin: 30px auto;
        width: 50%;
        text-align: left;
       }
      ul {
        list-style-type: circle;
        padding: 20px;
      }
      h1 {
        font-weight: bold;
        color: #FFFFFF;
        font-size: 24px;
      }
    
      h2 {
        font-weight: bold;
        color: #FFFFFF;
        font-size: 32px;
      }
      @media (max-width: 800px) {
        .text_container {
          width: 80%;
        }
      }
    `}</style>
    </Layout>
  )
}

export default CrewFire