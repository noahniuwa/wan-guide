import Layout from "../components/layout"
import VideoGuide from "../components/videoGuide"


const RedPackageMobile = () =>{
  return (
    <Layout>
      <VideoGuide
      pageTitle="Red Package Mobile Video Guide"
      videoTitle="Red Package Mobile"
      videoUrl="https://www.youtube.com/embed/Dvk-KAGZHGE"
      info="This video is a quick explanation of the Red Package Mobile DApp."
      ></VideoGuide>
    </Layout>
  )
}

export default RedPackageMobile 