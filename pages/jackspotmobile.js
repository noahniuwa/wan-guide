import Layout from "../components/layout"
import VideoGuide from "../components/videoGuide"

const JacksPotMobile = () =>{
  return (
    <Layout>
      <VideoGuide
      pageTitle="Jack's Pot Mobile Video Guide"
      videoTitle="Jack's Pot Mobile"
      videoUrl="https://www.youtube.com/embed/qua9pRF7FAE"
      info="This video is a quick explanation of the Jack's Pot Mobile DApp."
      ></VideoGuide>
    </Layout>
  )
}

export default JacksPotMobile