import Layout from "../components/layout"
import VideoGuide from "../components/videoGuide"

const ImportKeystore = () =>{
  return (
    <Layout>
      <VideoGuide
      pageTitle="Import Keystore file on Wanchain Wallet Guide"
      videoTitle="Import Keystore File"
      videoUrl="https://www.youtube.com/embed/6vAAZB8SZVw"
      info="This video is a guide on how to import a Keystore file into a new, or already created wanchain wallet."
      />
     
    </Layout>
  )
}

export default ImportKeystore