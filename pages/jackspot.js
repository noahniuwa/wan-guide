import Layout from "../components/layout"
import VideoGuide from "../components/videoGuide"


const JacksPot = () =>{
  return (
    <Layout>
      <VideoGuide
      pageTitle="Jack's Pot Video Guide"
      videoTitle="Jack's Pot"
      videoUrl="https://www.youtube.com/embed/0i-yYCF--T0"
      info="This video is a quick explanation of the Jack's Pot DApp."
      ></VideoGuide>
    </Layout>
  )
}

export default JacksPot 